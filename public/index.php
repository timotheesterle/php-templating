<?php

require_once __DIR__.'/../vendor/autoload.php';
$loader = new \Twig\Loader\FilesystemLoader('views');
$twig = new \Twig\Environment($loader, [
    'cache' => 'cache',
    'auto_reload' => true
]);

$faker = Faker\Factory::create();

$template = $twig->load('company.twig');

echo $template->render([
    'company' => $faker->company,
    'catchPhrase' => $faker->catchPhrase,
    'product' => $faker->bs,
    'productAdjective' => $faker->bs,
    'productMaterial' => $faker->bs,
    'colorName' => $faker->colorName,
    'price' => $faker->randomNumber(2),
    'url' => $faker->url,
    'productImage' => $faker->imageUrl($width = 200, $height = 200, 'sports'),
    'userImage' => $faker->imageUrl($width = 200, $height = 200, 'cats'),
    'userName' => $faker->firstName,
    'job' => $faker->jobTitle,
    'email' => $faker->email,
    'phoneNumber' => $faker->phoneNumber,
    'address' => $faker->address
]);

?>
